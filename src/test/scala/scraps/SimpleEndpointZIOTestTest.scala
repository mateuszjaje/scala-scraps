package scraps

import io.circe.Json
import sttp.client3.circe.asJson
import sttp.client3.impl.zio.RIOMonadAsyncError
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{basicRequest, UriContext}
import sttp.tapir.server.stub.TapirStubInterpreter
import sttp.tapir.ztapir._
import zio.ZIO
import zio.test.Assertion._
import zio.test.{ZIOSpecDefault, _}

import java.time.{ZoneId, ZonedDateTime}

object SimpleEndpointZIOTestTest extends ZIOSpecDefault {
  val sampleData = ResourceView("1", 2, ZonedDateTime.of(2020, 10, 5, 12, 34, 45, 0, ZoneId.of("UTC")))

  val retrievalServerEndpoint = SimpleEndpoint.retrieveEndpoint
    .zServerLogic(_ => ZIO.succeed(sampleData))

  val underTest = TapirStubInterpreter(SttpBackendStub(new RIOMonadAsyncError[Any]))
    .whenServerEndpoint(retrievalServerEndpoint)
    .thenRunLogic()
    .backend()

  val req = basicRequest
    .get(uri"http://test.com/some-resource")
    .response(asJson[Json])

  lazy val spec = suite("SimpleEndpoint")(
    test("should return some value") {
      for {
        response         <- underTest.send(req)
        responsePayload  <- ZIO.fromEither(response.body)
        unpackedResponse <- ZIO.fromEither(responsePayload.asObject.toRight(???).map(_.toMap))
      } yield assert(response.code.code)(equalTo(200)) &&
        assert(unpackedResponse)(
          equalTo(
            Map(
              "id"           -> Json.fromString("1"),
              "value"        -> Json.fromLong(2),
              "anotherValue" -> Json.fromString("2020-10-05T12:34:45Z[UTC]"),
            ),
          ),
        )
    },
  )

}
