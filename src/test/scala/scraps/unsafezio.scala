package scraps

import zio.{Task, UIO, Unsafe}

trait unsafezio {
  val runtime = zio.Runtime.default

  implicit class ExecuteZIO[A](effect: Task[A]) {
    def execute: A = Unsafe.unsafe(implicit unsafe => runtime.unsafe.run(effect).getOrThrowFiberFailure())
  }

}
