package scraps

import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.io.File

class AppCliConfigurationParserTest extends AnyFlatSpec with Matchers with EitherValues {

  behavior of "AppCliConfigurationParser"

  it should "parse correct input" in {
    AppCliConfigurationParser.parse(Seq("--input-data", "./somefile")).value shouldBe
      AppCliConfiguration(Some(new File("./somefile")))
    AppCliConfigurationParser.parse(Seq("--input-data", "./somefile", "--dry-run")).value shouldBe
      AppCliConfiguration(Some(new File("./somefile")), dryRun = true)
  }

  it should "fail on invalid dry-run flag" in {
    AppCliConfigurationParser.parse(Seq("--input-data", "./somefile", "--dry-run", "true")) shouldBe Symbol("left")
  }
}
