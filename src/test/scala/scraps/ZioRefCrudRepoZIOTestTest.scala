package scraps

import zio.ZIO
import zio.test._

object ZioRefCrudRepoZIOTestTest extends ZIOSpecDefault {

  val spec =
    suite("ZioRefCrudRepoZIOTest spec")(
      test("store data") {
        for {
          storage <- ZIO.service[SomeCrudRepo[String, Entity]]
          _       <- storage.put(Entity("key1", "dsa"))
          result  <- storage.getAll
        } yield assertTrue(result == Vector(Entity("key1", "dsa")))
      },
      test("alter data") {
        for {
          storage      <- ZIO.service[SomeCrudRepo[String, Entity]]
          _            <- storage.put(Entity("key1", "dsa"))
          updateResult <- storage.updateEntity("key1", _.copy(value = "another"))
          result       <- storage.getAll
          resultingEntity = Entity("key1", "another")
        } yield assertTrue(result == Vector(resultingEntity)) &&
          assertTrue(updateResult.contains(Entity("key1", "dsa")))
      },
    ).provide(ZioRefCrudRepo.make[String, Entity])

}
