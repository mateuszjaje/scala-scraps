package scraps

import cats.syntax.either._
import better.files.File
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import zio.{ZIO, ZLayer}
import zio.test._
import zio.test.Assertion._

import scala.util.Try

object JsonFileBasedCrudRepoZIOTestTest extends ZIOSpecDefault {
  implicit val EntityCirceCodec: Codec[Entity] = deriveCodec

  val tmpDirL = ZLayer(ZIO.fromEither(Either.catchNonFatal(File.newTemporaryDirectory())))
  val storageL: ZLayer[Any, Throwable, SomeCrudRepo[String, Entity]] =
    tmpDirL >>> ZLayer.fromFunction(new JsonFileBasedCrudRepo[String, Entity](_))

  val spec =
    suite("JsonFileBasedCrudRepo spec")(
      test("store data") {
        for {
          tmpDir           <- ZIO.service[File]
          storage          <- ZIO.service[SomeCrudRepo[String, Entity]]
          _                <- storage.put(Entity("key1", "dsa"))
          result           <- storage.getAll
          filesUnderTmpDir <- ZIO.fromTry(Try(tmpDir.children.size))
        } yield assert(result)(equalTo(Vector(Entity("key1", "dsa")))) &&
          assert(filesUnderTmpDir)(equalTo(1))
      }.provideLayer(tmpDirL and storageL),
      test("alter data") {
        for {
          storage      <- ZIO.service[SomeCrudRepo[String, Entity]]
          _            <- storage.put(Entity("key1", "dsa"))
          updateResult <- storage.updateEntity("key1", _.copy(value = "another"))
          result       <- storage.getAll
          resultingEntity = Entity("key1", "another")
        } yield assertTrue(result == Vector(resultingEntity)) &&
          assertTrue(updateResult.contains(Entity("key1", "dsa")))
      }.provideLayer(storageL),
    )

}
