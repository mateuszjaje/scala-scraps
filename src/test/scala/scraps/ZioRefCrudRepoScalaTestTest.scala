package scraps

import org.scalatest.OptionValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import zio.ZIO

case class Entity(key: String, value: String) extends EntityKey[String]

class ZioRefCrudRepoScalaTestTest extends AnyFlatSpec with Matchers with unsafezio with OptionValues {

  behavior of "ZioRefCrudRepoScalaTest"

  it should "store data" in {
    val a = for {
      storage <- ZIO.service[SomeCrudRepo[String, Entity]]
      _       <- storage.put(Entity("key1", "dsa"))
      result  <- storage.getAll
    } yield result

    a.provide(ZioRefCrudRepo.make[String, Entity]).execute shouldBe Vector(Entity("key1", "dsa"))
  }

  it should "update entity" in {
    val (updateResult, result) = (for {
      storage      <- ZIO.service[SomeCrudRepo[String, Entity]]
      _            <- storage.put(Entity("key1", "dsa"))
      updateResult <- storage.updateEntity("key1", _.copy(value = "another"))
      result       <- storage.getAll
    } yield updateResult -> result).provide(ZioRefCrudRepo.make[String, Entity]).execute

    updateResult.value shouldBe Entity("key1", "dsa")
    result shouldBe Vector(Entity("key1", "another"))
  }

}
