package scraps

import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Seconds, Span}
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{basicRequest, UriContext}
import sttp.tapir.server.stub.TapirStubInterpreter

import java.time.{ZoneId, ZonedDateTime}
import scala.concurrent.Future

class SimpleEndpointScalaTestTest extends AnyFlatSpec with Matchers with ScalaFutures {

  val sampleData = ResourceView("1", 2, ZonedDateTime.of(2020, 10, 5, 12, 34, 45, 0, ZoneId.of("UTC")))

  val se = SimpleEndpoint.retrieveEndpoint.serverLogicSuccess(_ => Future.successful(sampleData))

  trait Ctx {
    val underTest = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
      .whenServerEndpoint(se)
      .thenRunLogic()
      .backend()

  }

  behavior of "SimpleEndpoint.retrieveEndpoint"

  it should "work" in new Ctx {
    // when
    val response = basicRequest
      .get(uri"http://test.com/some-resource")
      .send(underTest)

    // then
    response.futureValue(Timeout(Span(5, Seconds))).body shouldBe Right(
      """{"id":"1","value":2,"anotherValue":"2020-10-05T12:34:45Z[UTC]"}""",
    )
  }
}
