package scraps

import org.http4s.HttpRoutes
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import sttp.tapir.server.http4s.ztapir.ZHttp4sServerInterpreter
import sttp.tapir.ztapir._
import zio.interop.catz._
import zio.{RIO, Ref, ZIO, ZLayer}

import java.time.ZonedDateTime

object ZIOZioHttp4sTapirServer extends zio.ZIOAppDefault {
  type Env = Ref[ResourceView]

  val ref: ZLayer[Any, Nothing, Ref[ResourceView]] = ZLayer.fromZIO(Ref.make(ResourceView("1", 2, ZonedDateTime.now())))

  val retrieveServerEndpoint =
    SimpleEndpoint.retrieveEndpoint
      .zServerLogic(_ => ZIO.service[Ref[ResourceView]].flatMap(_.get))
      .widen[Env]

  val replaceServerEndpoint =
    SimpleEndpoint.createEndpoint
      .zServerLogic { input =>
        val newValue = ResourceView(input._1, input._2.value, input._2.anotherValue)
        ZIO.service[Ref[ResourceView]].flatMap(_.set(newValue).as(newValue))
      }
      .widen[Env]

  val serviceEndpoints: List[ZServerEndpoint[Env, Any]] = List(
    retrieveServerEndpoint,
    replaceServerEndpoint,
  )

  val swaggerUIEndpoints = SwaggerRoute.swaggerUIZIO.map(_.widen[Env])

  val routes: HttpRoutes[RIO[Env, *]] =
    ZHttp4sServerInterpreter().from(serviceEndpoints ++ swaggerUIEndpoints).toRoutes

  val server = ZIO.executor.flatMap { executor =>
    BlazeServerBuilder[RIO[Env, *]]
      .withExecutionContext(executor.asExecutionContext)
      .bindHttp(8080, "localhost")
      .withHttpApp(Router("/" -> routes).orNotFound)
      .serve
      .compile
      .drain
  }

  override val run = server.provide(ref)
}
