package scraps

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import sttp.apispec.openapi.OpenAPI
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.generic.auto.SchemaDerivation
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import zio.Task

import java.time.ZonedDateTime
import scala.concurrent.Future

case class ResourceView(
    id: String,
    value: Int,
    anotherValue: ZonedDateTime,
)

object ResourceView extends SchemaDerivation {
  implicit val circeCodec: Codec[ResourceView]   = deriveCodec[ResourceView]
  implicit val tapirSchema: Schema[ResourceView] = schemaForCaseClass[ResourceView].value
}

case class CreateResourceView(
    value: Int,
    anotherValue: ZonedDateTime,
)

object CreateResourceView extends SchemaDerivation {
  implicit val circeCodec: Codec[CreateResourceView]   = deriveCodec
  implicit val tapirSchema: Schema[CreateResourceView] = schemaForCaseClass[CreateResourceView].value
}

case class SomeError(errorCode: Int, message: String)

object SomeError extends SchemaDerivation {
  implicit val circeCodec: Codec[SomeError]   = deriveCodec
  implicit val tapirSchema: Schema[SomeError] = schemaForCaseClass[SomeError].value
}

case class InternalError(throwableMessage: String)

object InternalError extends SchemaDerivation {
  implicit val circeCodec: Codec[InternalError]   = deriveCodec
  implicit val tapirSchema: Schema[InternalError] = schemaForCaseClass[InternalError].value

}

object SimpleEndpoint {

  val resourceId = path[String]("resource-id").description("Identifier of a resource.")

  val resourceEndpoint =
    endpoint
      .out(statusCode(StatusCode.Ok).and(jsonBody[ResourceView]))
      .errorOut(
        oneOf(
          oneOfVariant(StatusCode.BadRequest, jsonBody[SomeError]),
          oneOfVariant(StatusCode.InternalServerError, jsonBody[InternalError]),
        ),
      )

  val retrieveEndpoint = resourceEndpoint
    .in("some-resource")
    .get

  val createEndpoint = resourceEndpoint
    .in("some-resource" / resourceId)
    .post
    .in(jsonBody[CreateResourceView])

}

object SwaggerRoute {
  // https://tapir.softwaremill.com/en/latest/docs/openapi.html

  val openApi: OpenAPI =
    OpenAPIDocsInterpreter().toOpenAPI(List(SimpleEndpoint.retrieveEndpoint, SimpleEndpoint.createEndpoint), "My API", "1.0")

  val swaggerUIFuture: List[ServerEndpoint[Any, Future]] =
    SwaggerInterpreter().fromEndpoints[Future](List(SimpleEndpoint.retrieveEndpoint, SimpleEndpoint.createEndpoint), "My API", "1.0")

  val swaggerUIZIO: List[ServerEndpoint[Any, Task]] =
    SwaggerInterpreter().fromEndpoints[Task](List(SimpleEndpoint.retrieveEndpoint, SimpleEndpoint.createEndpoint), "My API", "1.0")

}
